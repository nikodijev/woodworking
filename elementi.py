#!/usr/bin/env python3

# jednokrilni gornji *
# jednokrilni donji *
# dvokrilni gornji *
# dvokrilni donji *
# rerna *
# fiokas
# ugaoni *

import os

frontovi = []
korpusi = []
lesoniti = []
police = []
abs_traka = 0
kant_traka = 0
kvadratura_frontova = 0
kvadratura_korpusa_i_polica = 0
kvadratura_lesonita = 0


def count(lista):
    new_list = {} 
    for item in lista:
        if item in new_list: 
            new_list[item] += 1
        else: 
            new_list[item] = 1
    return new_list


def helping():
    os.system("clear")
    print('''
   y^
    |
    |
    |
    |----------->x
   /
  /
 /
/z

kod pozicija, moguce kombinacije za kant: npr. 50 x 24 |, ||, -, =, |=, |=|

|   : kantuje se jedna duzna strana
||  : dve duze strane
-   : jedna kraca strana
=   : dve krace strane
|=  : jedna duza i dve krace
|=| : sve cetiri strane se kantuju
''')
    print("\n")
    input("Press [ENTER] to continue...")
    os.system("clear")


def total():
    os.system("clear")

    if not police:
        if not frontovi:
            for korpus, lesonit in zip(korpusi, lesoniti):
                f = {}
                k = count(korpusi)
                l = count(lesoniti)
                p = {}
    if not police:
        for front, korpus, lesonit in zip(frontovi, korpusi, lesoniti):
            f = count(frontovi)
            k = count(korpusi)
            l = count(lesoniti)
            p = {}
    else:
        for front, korpus, lesonit, polica in zip(frontovi, korpusi, lesoniti, police):
            f = count(frontovi)
            k = count(korpusi)
            l = count(lesoniti)
            p = count(police)

    if kant_traka != 0:
        print("frontovi: ")
        for item in f:
                print('{} {}kom'.format(item, f[item]))
        print("\n")
        print("korpusi: ")
        for item in k:
            print('{} {}kom'.format(item, k[item]))
        print("\n")
        print("lesoniti: ")
        for item in l:
            print('{} {}kom'.format(item, l[item]))
        print("\n")
        print("police: ")
        for item in p:
            print('{} {}kom'.format(item, p[item]))
        print("\n")
        print("abs_traka: {:.2f}m".format(abs_traka))
        print("kant_traka: {:.2f}m".format(kant_traka))
        print("kvadratura_frontova: {:.2f}m2".format(kvadratura_frontova))
        print("kvadratura_korpusa_i_polica: {:.2f}m2".format(kvadratura_korpusa_i_polica))
        print("kvadratura_lesonita: {:.2f}m2".format(kvadratura_lesonita))
        print("\n")
        input("Press [ENTER] to continue...")
        os.system("clear")
    else:
        print("Nothing to show for now.")
        print("\n")
        input("Press [Enter] to continue...")
        os.system("clear")


def get_element_dimensions():
    global element
    x = float(format(validate_float_input("duzina(cm) po x-osi: "), '.2f'))
    y = float(format(validate_float_input("duzina(cm) po y-osi: "), '.2f'))
    z = float(format(validate_float_input("duzina(cm) po z-osi: "), '.2f'))
    n = validate_int_input("broj polica: ")
    if element == "5": 
        x1 = float(format(validate_float_input("duzina(cm) po x1-osi: "), '.2f'))
        os.system("clear")
        return [x, x1, y, z, n]
    os.system("clear")
    return [x, y, z, n]


def get_element():
    global element
    print("choose your element: \n")
    print("1)jednokrilni_gornji\n2)jednokrilni_donji\n3)dvokrilni_gornji\n4)dvokrilni_donji\n5)ugaoni\n6)fiokas\n7)rerna\n\n_______________________________________\nq:Quit\ts:Save\tt:Total\t h:Help\n---------------------------------------")
    element=input("-> ")
    os.system("clear")
    return element


def validate_float_input(prompt):
    while True:
        try:
            value = float(input(prompt))
        except ValueError:
            print("* unesi broj (ex: 100, 34.2, 3)")
            continue
        if value <= 0:
            print("* unesi pozitivan broj veci od nule (ex: 100, 34.2, 3)")
            continue
        else:
            break
    return value


def validate_int_input(prompt):
    while True:
        try:
            value = int(input(prompt))
        except ValueError:
            print("* unesi ceo broj (ex: 2, 4, 6)")
            continue
        if value < 0:
            print("* unesi pozitivan broj (ex: 100, 34.2, 3)")
            continue
        else:
            break
    return value


def jednokrilni_gornji():
    global frontovi
    global korpusi
    global lesoniti
    global police
    global abs_traka
    global kant_traka
    global kvadratura_frontova
    global kvadratura_korpusa_i_polica
    global kvadratura_lesonita

    x,y,z,n = get_element_dimensions()
    
    front = '{:.2f} x {:.2f}'.format(x-0.1, y-0.1)

    korpus_bocna = '{:.2f} x {:.2f}'.format(y, z)
    korpus_duzna = '{:.2f} x {:.2f}'.format(x-3.6, z)

    lesonit = '{:.2f} x {:.2f}'.format(x, y)

    if n != 0:
        polica = '{:.2f} x {:.2f}'.format(x-3.8, z-2)
        for _ in range(n):
            police.append(polica)
    
    frontovi.append(front)
    
    for _ in range(2):
        korpusi.append(korpus_bocna)
        korpusi.append(korpus_duzna)
    
    lesoniti.append(lesonit)

    abs_traka += (2*(x-0.1) + 2*(y-0.1))/100
    kant_traka += (2*(x-3.6) + 2*(y) + 4*(z))/100
    
    kvadratura_frontova += ((x-0.1)*(y-0.1))/10000
    kvadratura_korpusa_i_polica += ((y*z)*2 + (x-3.6)*z*2 + ((x-3.8)*(z-2))*n)/10000
    kvadratura_lesonita += (x*y)/10000
    
    print("front:",front)
    print("korpus: {} 2kom, {} 2kom".format(korpus_bocna, korpus_duzna))
    print("lesonit:",lesonit)
    if n == 0:
        print("police: 0kom")
    else:
        print("police: {} {}kom".format(polica,n))
    print("\n")
     
def jednokrilni_donji():
    global frontovi
    global korpusi
    global lesoniti
    global police
    global abs_traka
    global kant_traka
    global kvadratura_frontova
    global kvadratura_korpusa_i_polica
    global kvadratura_lesonita

    x,y,z,n = get_element_dimensions()
    
    front = '{:.2f} x {:.2f}'.format(x-0.1, y-0.1)

    korpus_bocna = '{:.2f} x {:.2f}'.format(y-1.8, z)
    korpus_duzna = '{:.2f} x {:.2f}'.format(x, z)
    korpus_sajtna = '{:.2f} x {:.2f}'.format(x-3.6, 8)

    lesonit = '{:.2f} x {:.2f}'.format(x, y)

    if n != 0:
        polica = '{:.2f} x {:.2f}'.format(x-3.8, z-2)
        for _ in range(n):
            police.append(polica)
    
    frontovi.append(front)
    
    for _ in range(2):
        korpusi.append(korpus_bocna)
        korpusi.append(korpus_sajtna)
    korpusi.append(korpus_duzna)
     
    lesoniti.append(lesonit)

    abs_traka += (2*(x-0.1) + 2*(y-0.1))/100
    kant_traka += (x + 2*(x-3.6) + 2*(y-1.8))/100
    
    kvadratura_frontova += ((x-0.1)*(y-0.1))/10000
    kvadratura_korpusa_i_polica += (((y-1.8)*z)*2 + x*z + (x-3.6)*8*2 + ((x-3.8)*(z-2))*n)/10000
    kvadratura_lesonita += (x*y)/10000
    
    print("front:",front)
    print("korpus: {} 2kom, {} 1kom, {} 2kom".format(korpus_bocna, korpus_duzna, korpus_sajtna))
    print("lesonit:",lesonit)
    if n == 0:
        print("police: 0kom")
    else:
        print("police: {} {}kom".format(polica,n))
    print("\n")


def dvokrilni_gornji():
    global frontovi
    global korpusi
    global lesoniti
    global police
    global abs_traka
    global kant_traka
    global kvadratura_frontova
    global kvadratura_korpusa_i_polica
    global kvadratura_lesonita

    x,y,z,n = get_element_dimensions()
    
    front = '{:.2f} x {:.2f}'.format((x-0.5)/2, y-0.1)

    korpus_bocna = '{:.2f} x {:.2f}'.format(y, z)
    korpus_duzna = '{:.2f} x {:.2f}'.format(x-3.6, z)

    lesonit = '{:.2f} x {:.2f}'.format(x, y)

    if n != 0:
        polica = '{:.2f} x {:.2f}'.format(x-3.8, z-2)
        for _ in range(n):
            police.append(polica)
      
    for _ in range(2):
        korpusi.append(korpus_bocna)
        korpusi.append(korpus_duzna)
        frontovi.append(front)
     
    lesoniti.append(lesonit)

    abs_traka += (4*((x-0.5)/2) + 4*(y-0.1))/100
    kant_traka += (2*(x-3.6) + 2*(y) + 4*z)/100
    
    kvadratura_frontova += ((((x-0.5)/2)*(y-0.1))*2)/10000
    kvadratura_korpusa_i_polica += (((y)*z)*2 + (x-3.6)*z*2 + ((x-3.8)*(z-2))*n)/10000
    kvadratura_lesonita += (x*y)/10000
    
    print("front:",front)
    print("korpus: {} 2kom, {} 2kom".format(korpus_bocna, korpus_duzna))
    print("lesonit:",lesonit)
    if n == 0:
        print("police: 0kom")
    else:
        print("police: {} {}kom".format(polica,n))
    print("\n")


def dvokrilni_donji():
    global frontovi
    global korpusi
    global lesoniti
    global police
    global abs_traka
    global kant_traka
    global kvadratura_frontova
    global kvadratura_korpusa_i_polica
    global kvadratura_lesonita

    x,y,z,n = get_element_dimensions()
    
    front = '{:.2f} x {:.2f}'.format((x-0.5)/2, y-0.1)

    korpus_bocna = '{:.2f} x {:.2f}'.format(y-1.8, z)
    korpus_duzna = '{:.2f} x {:.2f}'.format(x, z)

    lesonit = '{:.2f} x {:.2f}'.format(x, y)

    if n != 0:
        polica = '{:.2f} x {:.2f}'.format(x-3.8, z-2)
        for _ in range(n):
            police.append(polica)
       
    for _ in range(2):
        korpusi.append(korpus_bocna)
        frontovi.append(front)
    korpusi.append(korpus_duzna)
     
    lesoniti.append(lesonit)

    abs_traka += (4*((x-0.5)/2) + 4*(y-0.1))/100
    kant_traka += (x+ 2*(y-1.8))/100
    
    kvadratura_frontova += ((((x-0.5)/2)*(y-0.1))*2)/10000
    kvadratura_korpusa_i_polica += (((y-1.8)*z)*2 + (x)*z + ((x-3.8)*(z-2))*n)/10000
    kvadratura_lesonita += (x*y)/10000
    
    print("front:",front)
    print("korpus: {} 2kom, {} 1kom".format(korpus_bocna, korpus_duzna))
    print("lesonit:",lesonit)
    if n == 0:
        print("police: 0kom")
    else:
        print("police: {} {}kom".format(polica,n))
    print("\n")


def rerna():
    global frontovi
    global korpusi
    global lesoniti
    global police
    global abs_traka
    global kant_traka
    global kvadratura_frontova
    global kvadratura_korpusa_i_polica
    global kvadratura_lesonita

    x,y,z,n = get_element_dimensions()

    korpus_bocna = '{:.2f} x {:.2f}'.format(y-1.8, z)
    korpus_duzna = '{:.2f} x {:.2f}'.format(x, z)
    korpus_sajtna = '{:.2f} x {:.2f}'.format(x-3.6, 8)
    korpus_dno = '{:.2f} x {:.2f}'.format(x-3.6, z-0.2)
    korpus_sajtnica = '{:.2f} x {:.2f}'.format(x-3.6, 8.4)

    lesonit = '{:.2f} x {:.2f}'.format(x, y)
       
    for _ in range(2):
        korpusi.append(korpus_bocna)
        korpusi.append(korpus_sajtna)
    korpusi.append(korpus_duzna)
    korpusi.append(korpus_dno)
    korpusi.append(korpus_sajtnica)
     
    lesoniti.append(lesonit)

    kant_traka += (x + 2*(y) + 2*(z) + (x-3.6)*2)/100
    
    kvadratura_korpusa_i_polica += ((x*z) + 2*(y-1.8)*z + (2*(x-3.6)*8) + (x-3.6)*(z-0.2) + (x-3.6)*8.4)/10000
    kvadratura_lesonita += (x*y)/10000
    
    print("front: 0kom")
    print("korpus: {} 2kom, {} 1kom, {} 2kom, {} 1kom, {} 1kom".format(korpus_bocna, korpus_duzna, korpus_sajtna, korpus_dno, korpus_sajtnica))
    print("lesonit:",lesonit)
    print("police: 0kom")
    print("\n")


def ugaoni():
    global frontovi
    global korpusi
    global lesoniti
    global police
    global abs_traka
    global kant_traka
    global kvadratura_frontova
    global kvadratura_korpusa_i_polica
    global kvadratura_lesonita

    x,x1,y,z,n = get_element_dimensions()
    
    front = '{:.2f} x {:.2f}'.format((x-z)-2.5, y)

    korpus_bocna = '{:.2f} x {:.2f}'.format(y, z)
    korpus_dole = '{:.2f} x {:.2f}'.format(x1, z)
    korpus_gore = '{:.2f} x {:.2f}'.format(x-z-2.5, z)
    korpus_sajtna = '{:.2f} x {:.2f}'.format(y-3.6, 10)

    lesonit = '{:.2f} x {:.2f}'.format(x, y)
    lesonit1 = '{:.2f} x {:.2f}'.format(x1, y)

    if n != 0:
        polica = '{:.2f} x {:.2f}'.format(x1, z-2)
        polica1 = '{:.2f} x {:.2f}'.format(x-z-2.5, z-2)
        for _ in range(n):
            police.append(polica)
            police.append(polica)
            police.append(polica1)
            police.append(polica1)
      
    for _ in range(2):
        korpusi.append(korpus_bocna)
        korpusi.append(korpus_gore)
        korpusi.append(korpus_dole)
        frontovi.append(front)
    korpusi.append(korpus_sajtna)
     
    lesoniti.append(lesonit)
    lesoniti.append(lesonit1)

    abs_traka += (4*(x-z-2.5) + 4*y)/100
    kant_traka += ((y-3.6)*2 + 2*(x-z) + 2*z + 2*(x-z)*n)/100
    
    kvadratura_frontova += ((x-z-2.5)*y*2)/10000
    kvadratura_korpusa_i_polica += ((y-3.6)*z*2 + (x1*z)*2 + (x-z)*z*2 + (y-3.6)*10 + (x1*(z-2) + (x-z)*(z-2))*n)/10000
    kvadratura_lesonita += ((x*y) + (x1*y))/10000
    
    print("front: {} 2kom".format(front))
    print("korpus: {} 2kom, {} 2kom, {} 2kom, {} 2kom".format(korpus_bocna, korpus_dole, korpus_gore, korpus_sajtna))
    print("lesonit: {} 1kom, {} 1kom".format(lesonit, lesonit1))
    if n == 0:
        print("police: 0kom")
    else:
        print("police: {} {}kom, {} {}kom".format(polica, n, polica1, n))
    print("\n")


def main():
    global element
    os.system("clear")
    element = get_element()
    
    while element != "q":
        if element == "s":
            filename = input("enter filename: ")
            saving()
            print("[*]saved to {}\n".format(filename))
        elif element == "t":
            total()
        elif element == "h":
            helping()
        elif element == "1":
            jednokrilni_gornji()
        elif element == "2":
            jednokrilni_donji()
        elif element == "3":
            dvokrilni_gornji()
        elif element == "4":
            dvokrilni_donji()
        elif element == "5":
            ugaoni()
        elif element == "6":
            fiokas()
        elif element == "7":
            rerna()
        else:
            print("no such element!\n")
        element = get_element()

    os.system("clear")

main()

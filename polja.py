#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Deljenje duzine na polja, npr. za police.')
parser.add_argument("d", type=float, help="duzina")
parser.add_argument("b", type=int, help="broj poprekih greda")
parser.add_argument("s", type=float, help="sirina popreke grede")
args = parser.parse_args()

polje1 = (args.d-(args.b*args.s))/(args.b+1)
polje2 = polje1+args.s
zbir = polje1+args.s
for i in range(args.b):
    print(f'{polje1:.2f} - {polje2:.2f}')
    polje1 += zbir
    polje2 += zbir

#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Vadjenje kanala na plotovima.')
parser.add_argument("l", type=float, help="duzina plota")
parser.add_argument("n", type=int, help="broj polja")
parser.add_argument("-d", "--distancer", type=float, help="sirina distancera", default=0)
args = parser.parse_args()

polje = args.l/args.n
distancer = args.distancer
prva_pozicija = polje - distancer
pozicija = prva_pozicija
print(f'{pozicija:.2f}')
for i in range(args.n-2):
    pozicija += polje
    print(f'{pozicija:.2f}')

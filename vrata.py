#!/usr/bin/env python3

import argparse

parser = argparse.ArgumentParser(description='Pozicije za vrata na osnovu otvora.')
parser.add_argument("v", type=float, help="visina otvora")
parser.add_argument("s", type=float, help="sirina otvora")
parser.add_argument("d", type=float, help="dubina otvora")
args = parser.parse_args()

def formatit(x):
    return (format((x), ".1f"))

popreka = args.s - 3
bocna = args.v - (1.5 + 1.8)
lajsna_duga = bocna + 7
lajsna_popreka = popreka - 3.6

plot_duzina = bocna - 0.8
plot_sirina = lajsna_popreka - 0.6

plot = ['plot:', formatit(plot_duzina), 'x', formatit(plot_sirina), '', '']
stok1 = ['stokovi:', formatit(bocna), 'x', str(args.d), '-', '2kom'] 
stok2 = ['', formatit(popreka), 'x', str(args.d), '-', '1kom'] 
lajsne1 = ['lajsne:', formatit(lajsna_duga), 'x', '7.0', '-', '2kom'] 
lajsne2 = ['', formatit(lajsna_popreka), 'x', '7.0', '-', '1kom'] 
pervajz1 = ['pervajz_lajsne:', formatit(lajsna_duga), 'x', '6.6', '-', '2kom'] 
pervajz2 = ['', formatit(lajsna_popreka), 'x', '6.6', '-', '1kom'] 

l = []
l.append(plot)
l.append(stok1)
l.append(stok2)
l.append(lajsne1)
l.append(lajsne2)
l.append(pervajz1)
l.append(pervajz2)

for row in l:
    print("{: <15} {: >5} {: >1}{: >5} {: >1} {: >4}".format(*row))
